#include<stdio.h>

int main()
{
    //Step-1 Declaration and Initialisation
    int principle = 0,Time = 0, RateOfInterest = 0;
    float simpleInterest = 0;
    printf("Enter the Principle, Time and Rate of Interest");
    //Step2 - Accept the values
    scanf("%d%d%d",&principle,&Time,&RateOfInterest);
    
    //Step3- Calculate simpleInterest
    simpleInterest = principle * Time  * RateOfInterest /100;
    
    //Step4- Display
    printf("%f is the simpleInterest",simpleInterest);
    return 0;
}