#include <stdio.h>

int main() {
   int n, m, i, k,sum = 0;
   printf("Enter two numbers: ");
   scanf("%d %d", &n, &m);
   printf("Prime numbers between %d and %d are: ", n, m);

   
   while (n <= m) {
      k = 0;

     
      if (n <= 1) {
         ++n;
         continue;
      }

      for (i = 2; i <= n / 2; ++i) 
      {

         if (n % i == 0) 
         {
            k = 1;
            break;
         }
      }

      if (k == 0)
         {
            printf("%d ", n);
            sum = n + sum;
         }
      ++n;
   }
     printf("\n The sum of prime numbers is: %d ", sum);
   return 0;
}