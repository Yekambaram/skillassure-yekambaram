#include <stdio.h>

int main()
{
    //step1: declaration and initialisation
    int num=0,sum=0,i;
    printf("\n enter the numbers:");
    
    //step2: Accept the numbers
    scanf("%d",&num);
    
    //step3: calculations
    for(i=0;i<=num;i++)
    {
        if(i%2 !=0)
         sum = sum+i;
    }
    
    //step4: display
    printf("\n The sum of odd numbers is : %d",sum);
    
    return 0;
}