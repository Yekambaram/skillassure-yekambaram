#include<stdio.h>

int main()
{
    //Step-1 Declaration and Initialisation
    int num1,num2,temp;
    //Step2 - Accept the values
    printf("\n Enter the numbers:");
    scanf("%d%d",&num1,&num2);
    printf("\n The numbers are : %d,%d",num1,num2);
    //Step3- Calculation
    temp = num1;
    num1= num2;
    num2= temp;
    //Step4- Display
    printf("\n The swapped numbers are %d,%d",num1,num2);

    return 0;
}